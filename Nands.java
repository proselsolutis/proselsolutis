package titan;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Nands - a robot by (your name here)
 */
public class Nands extends AdvancedRobot
{
	/**
	 * run: Nands's default behavior
	 */
	public void run() {
	
	

		setColors(Color.red,Color.red,Color.blue); // body,gun,radar

		// Robot main loop
		while(true) {
				
			setAhead(100);
			setTurnRight(130);
			setTurnGunLeft(90);
			execute();		
			
	
			}
			
	}


	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Replace the next line with any behavior you would like
		fire(1);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		back(10);
					
			
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		back(10);
		turnRight(10);
			
	}	
}
